import React from 'react'
import PropTypes from "prop-types"

class PayPal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            editModelShow: false,
        };
    }
    componentWillMount() {
        const that = this
        if(!this.state.editModelShow) {
            this.setState({
                editModelShow: true,
            },() => {
                paypal.Button.render({

                    env: 'sandbox', // sandbox | production
                    client: {
                        sandbox:    'AcMxLr-HOsgTEVoiLVd5wTXGsXsrzr2dIyMjJxGSqDZd7huSh6Tue9r8OS9-O45QoBwQzsJ6wm6OGxi_',
                        production: 'AXsGhTo5jCqobjwsA_niVqqiXtOuuImsjuj9vpfVZIf6unl75F1ZuQTddF4TRKkXsQEbOXN03xp0Dr1C'
                    },

                    commit: true,

                    payment: function(data, actions) {

                        // Make a call to the REST api to create the payment
                        return actions.payment.create({
                            payment: {
                                transactions: [
                                    {
                                        amount: { total: parseInt(that.props.total || 0,10), currency: 'USD' }
                                    }
                                ]
                            }
                        });
                    },

                    onAuthorize: function(data, actions) {

                        // Make a call to the REST api to execute the payment
                        return actions.payment.execute().then(function() {
                            console.log("================>", data)
                            window.alert('Payment Complete!');
                        });
                    }

                }, `#${this.props.paypalId}`);
            })
        }

    }

    render() {
        return(
            <div id={this.props.paypalId}/>
        )
    }
}

PayPal.propTypes = {
    paypalId: PropTypes.string.isRequired,
    total: PropTypes.number.isRequired
};

/*PayPal.defaultProps = {
    env: 'sandbox',
    onSuccess: (payment) => {
        console.log('The payment was succeeded!', payment);
    },
    onCancel: (data) => {
        console.log('The payment was cancelled!', data);
    },
    onError: (err) => {
        console.log('Error loading Paypal script!', err);
    },
};*/

export default PayPal;