import React from 'react';
import Header from './header';
import Footer from './footer';
import {Row, Col, Icon, Button} from 'antd'
import axios from 'axios';
import BaseUrl from '../config/properties';
import PaypalExpressBtn from 'react-paypal-express-checkout';
import PayPal from "./PayPal";
require('../styles/courses.css');

const ButtonGroup = Button.Group;

class courseIntro extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editModelShow: false,
    };
  }

  isEditModel(){
    this.setState({
      editModelShow: true
    })
  }

  componentWillMount() {
      // const courseName = window.location.pathname.replace('/','')
      const { courseName } = this.props.location.state
      console.log(name)
      let isLoggedIn = JSON.parse(localStorage.getItem('isLoggedIn'));
      let authToken = null;
      let userData = [];
      if (isLoggedIn == true)  {
          authToken = localStorage.getItem('authToken');
          userData = JSON.parse(localStorage.getItem('userData'));
          console.log(userData);
          this.setState({
              authToken:authToken,
              userData:userData,
              url:userData.picture.data.url
          });
      }
      if (authToken) {
          const config = {
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: localStorage.getItem('authToken')
              }
          }
          axios.get(BaseUrl.base_url + `/api/v1/Courses/Name/${courseName}/Details`, config).then((response) => {
              const courseDescription = {};
              if (response.data) {
                  this.setState({
                      courseDescription: response.data,
                  },() => {this.props.history.push(`/course/${courseName}`)})
              }
          }).catch((error) => {
              this.setState({error: error.message})
          });
      }
  }

  handleBack() {
      this.setState({
      },() => {
          this.props.history.push({
              pathname : `/course`,
          })
      })
  }

  handleLessons() {
      const {courseDescription} = this.state
      this.setState({
      },() => {
          this.props.history.push({
              pathname : `/course/${courseDescription.microCourse_Name}/Lesson`,
              state: { courseName: courseDescription.microCourse_Name }
          })
      })
  }

  render() {
    const { courseDescription } = this.state

      const onSuccess = (payment) => {
          console.log("The payment was succeeded!", payment);
      }

      const onCancel = (data) => {
          console.log('The payment was cancelled!', data);
      }

      const onError = (err) => {
          console.log("Error!", err);
      }

      let env = 'sandbox';
      let currency = 'INR';
      let total = courseDescription && courseDescription.microCourse_Price;

      const client = {
          // sandbox:    'AZcIgtkNrGV0wv5VECuIq3Irm3xB7YZjY6T90R92TMSr70dlaxEcjVK4nRNnLu6MCN3STNdP6YWUdmtw',
          sandbox:    'AYtnAl7gkUzvrKgM_D2X2adNRS_WuhOTC7XkkcWILEHP_ajb4QAj3zuDB2DsvOeuvgXR7JOZ9fp0fq2I',
          production: 'AXsGhTo5jCqobjwsA_niVqqiXtOuuImsjuj9vpfVZIf6unl75F1ZuQTddF4TRKkXsQEbOXN03xp0Dr1C',
      }

    return (
        <div>
            <div className="main_div">
                <Header/>
            </div>
            <div className="container">
                <div className="col-md-12 col-sm-12 col-xs-12 article_content">
                    <h2 className="article_innercont">Course Description</h2>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 article_content">
                    <div>
                        <div className={'col-md-12 col-sm-12'}>
                            <div className="block_locker course-body">
                                <div className="col-md-12 col-sm-12 col-xs-12 block_image account-pg">
                                    {
                                        courseDescription ?
                                            <Row gutter={8}>
                                                <Col span={4} className="text-center">
                                                    <img height={125} src={courseDescription && courseDescription.microCourse_ImageURL }
                                                         className="course-image"/>
                                                </Col>
                                                <Col span={20}>
                                                    <div className="form-row">
                                                        <div className="form-group col-md-6">
                                                            <p className="intro-header">Name:</p>
                                                            <p className="intro-content">{courseDescription && courseDescription.microCourse_Name}</p>
                                                        </div>
                                                        <div className="form-group col-md-6">
                                                            <p className="intro-header">Author:</p>
                                                            <p className="intro-content">{courseDescription && courseDescription.microCourseCreator_Name}</p>
                                                        </div>
                                                    </div>

                                                    <div className="form-group col-md-12">
                                                        <p className="intro-header">Tags:</p>
                                                        <p className="intro-content">{courseDescription && courseDescription.microCourse_Tags}</p>
                                                    </div>

                                                    <div className="form-row">
                                                        <div className="form-group col-md-6">
                                                            <p className="intro-header">Lessons:</p>
                                                            <p className="intro-content">{courseDescription && courseDescription.microLessonsList.length}</p>
                                                        </div>
                                                        <div className="form-group col-md-6">
                                                            <p className="intro-header">Quiz:</p>
                                                            <p className="intro-content">{courseDescription && courseDescription.numQuizzes}</p>
                                                        </div>

                                                    </div>

                                                    <div className="form-group col-md-12">
                                                        <p className="intro-header">Course Description:</p>
                                                        <p className="intro-content">{courseDescription && courseDescription.microCourse_Description}</p>
                                                    </div>
                                                    <div className="form-row col-md-12 text-center intro-button">
                                                        {/*<Button type="primary">Paypal <i className="fa fa-paypal"/></Button>*/}
                                                        {/*<PaypalExpressBtn env={env} client={client} currency={currency} total={total} onError={onError} onSuccess={onSuccess} onCancel={onCancel} />*/}
                                                        <PayPal paypalId={`paypal_${courseDescription.microCourse_Id}`} total={parseInt(courseDescription.microCourse_Price || 1,10)}/>
                                                    </div>
                                                    <div className="form-row col-md-12 text-center intro-button">
                                                        <ButtonGroup>
                                                            <Button type="primary" onClick={() => this.handleBack()}>
                                                                <Icon type="left" />Back to Courses
                                                            </Button>
                                                            <Button type="primary" onClick={() => this.handleLessons()}>
                                                                View Lessons<Icon type="right" />
                                                            </Button>
                                                        </ButtonGroup>
                                                    </div>
                                                </Col>
                                            </Row>
                                        : null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>

    )
  }
}

export default courseIntro;
