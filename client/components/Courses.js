import React from 'react';
import Header from './header';
import Footer from './footer';
import axios from 'axios';
import BaseUrl from '../config/properties';
import PayPal from "./PayPal";
require('../styles/courses.css');

const CLIENT = {
    sandbox: 'AZcIgtkNrGV0wv5VECuIq3Irm3xB7YZjY6T90R92TMSr70dlaxEcjVK4nRNnLu6MCN3STNdP6YWUdmtw',
};

class Courses extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editModelShow: false,
    };
  }

  isEditModel(){
    this.setState({
      editModelShow: true
    })
  }

  componentWillMount() {
      let isLoggedIn = JSON.parse(localStorage.getItem('isLoggedIn'));
      let authToken = null;
      let userData = [];
      if (isLoggedIn == true)  {
          authToken = localStorage.getItem('authToken');
          userData = JSON.parse(localStorage.getItem('userData'));
          console.log(userData);
          this.setState({
              authToken:authToken,
              userData:userData,
              url:userData.picture.data.url
          });
      }
      if (authToken) {
          const config = {
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: localStorage.getItem('authToken')
              }
          }
          axios.get(BaseUrl.base_url+"/api/v1/Courses/ListAllCourses", config).then((response) => {
              this.setState({
                  allCourse: response.data.length && response.data
              })
          }).catch((error) => {
              this.setState({error:error.message})
          });
      }
  }


  courseNameDetails(courseName) {
      // const cName = courseName && courseName.replace(/'+'/g," ")
      /*const re = /'+'/gi;
      const newstr = courseName.replace(re, ' ');*/
      this.setState({
      },() => {
          this.props.history.push({
              pathname : `/course/${courseName}`,
              state: { courseName: courseName }
          })
      })
  }
    onClick () {
     this.setState ({
         showPayPal: true
     })
    }

  render() {
    const { allCourse } = this.state
    const Courses = this.props.courseInfo || {}

      const onSuccess = (payment) => {
          console.log("The payment was succeeded!", payment);
      }

      const onCancel = (data) => {
          console.log('The payment was cancelled!', data);
      }

      const onError = (err) => {
          console.log("Error!", err);
      }

      let env = 'sandbox';
      let currency = 'INR'
      let total = '0.01'

    return (
        <div>
            <div className="main_div">
                <Header/>
            </div>
            <div className="container">
                <div className="col-md-12 col-sm-12 col-xs-12 article_content">
                    <h2 className="article_innercont">List of Courses</h2>
                </div>
                <div className="col-md-12 col-sm-12 col-xs-12 article_content">
                    <div>
                        <div className={'col-md-12 col-sm-12'}>
                            <div className="block_locker course-body">
                                <div className="col-md-12 col-sm-12 col-xs-12 block_image account-pg">
                                    {
                                        allCourse && allCourse.map((item,i) => (
                                                <div key={i.toString()} className="col-md-6">
                                                    <div className="panel panel-default course-box">
                                                        <div className="row course-div">
                                                                <div className="course-name" onClick={() => this.courseNameDetails(item.microCourse_Name)}>
                                                                    <h4 className="course-name is-link">{item && item.microCourse_Name}</h4>
                                                                </div>
                                                                <div>
                                                                    <div className="text-center">
                                                                        <img className="course-img text-center" src={item && item.microCourse_ImageURL }/>
                                                                    </div>

                                                                     <h4 className="course-author">Author:</h4>
                                                                     <p className="course-content">{item && item.microCourseCreator_Name}</p><br/>
                                                                     <h4 className="course-content">Tags:</h4>
                                                                     <p className="course-content">{item && item.microCourse_Tags.replace(/,/g,", ")}</p><br/>
                                                                </div>
                                                            <div className="paypal-button text-center">
                                                                {/*<PayPal paypalId={`paypal_${item.microCourse_Id}`} total={parseInt(item.microCourse_Price || 1,10)}/>*/}
                                                            </div>
                                                                {/*<div className="paypal-button text-center">
                                                                    <PaypalButton
                                                                        client={CLIENT}
                                                                        env={env}
                                                                        commit={true}
                                                                        currency={currency}
                                                                        total={total}
                                                                        onSuccess={onSuccess}
                                                                        onError={onError}
                                                                        onCancel={onCancel}
                                                                    />
                                                                    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                                                        <input type="hidden" name="cmd" value="_s-xclick"/>
                                                                            <input type="hidden" name="hosted_button_id" value="R3DGCSL9LGJ46"/>
                                                                                <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynow_LG.gif" name="submit" alt="PayPal – The safer, easier way to pay online!"/>
                                                                                    <img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1"/>
                                                                    </form>

                                                                </div>*/}
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
  }
}

export default Courses;
