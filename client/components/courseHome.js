import React from 'react';
import Header from './header';
import Footer from './footer';
import {Row, Col, Icon, Modal, Button, Menu} from 'antd'
import axios from 'axios';
import BaseUrl from '../config/properties';
require('../styles/courses.css');

const ButtonGroup = Button.Group;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class courseHome extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editModelShow: false,
        key: "Intro"
    };
  }

  isEditModel(){
    this.setState({
      editModelShow: true
    })
  }

  componentWillMount() {
      const { courseName } = this.props.location.state
      //const course = courseName.replace(/,/g,", ")}
      let isLoggedIn = JSON.parse(localStorage.getItem('isLoggedIn'));
      let authToken = null;
      let userData = [];
      if (isLoggedIn == true)  {
          authToken = localStorage.getItem('authToken');
          userData = JSON.parse(localStorage.getItem('userData'));
          console.log(userData);
          this.setState({
              authToken:authToken,
              userData:userData,
              url:userData.picture.data.url
          });
      }
      if (authToken) {
          const config = {
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: localStorage.getItem('authToken')
              }
          }
          axios.get(BaseUrl.base_url + `/api/v1/Courses/Name/${courseName}/Details`, config).then((response) => {
              const courseDescription = {};
              if (response.data) {
                  this.setState({
                      courseDescription: response.data,
                  })
              }
          }).catch((error) => {
              this.setState({error: error.message})
          });
      }
  }


  handleClick(e) {
        console.log('click ', e);
  }

  handleBack() {
      this.setState({
      },() => {
          this.props.history.push({
              pathname : `/course`,
          })
      })
  }

  viewLessons (item, key) {
      const config = {
          headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: localStorage.getItem('authToken')
          }
      }
      axios.get(BaseUrl.base_url + `/api/v1/Lesson/${item.microLesson_Id}/Contents`, config).then((response) => {
          const lessonContent = {};
          if (response.data) {
              this.setState({
                  lessonContent: response.data,
              }, () => this.runCodePrettify())
          }
      }).catch((error) => {
          this.setState({error: error.message})
      });
    this.setState({
        lesson: item,
        key: key
    })
  }

  showMenu (item, key) {
      return (
          <div>
              <Col span={18}>
                  <div className="form-row col-md-12">
                      <ButtonGroup>
                          <Button type="primary" onClick={() => this.handleBack()}>
                              <Icon type="left" />Back to Courses
                          </Button>
                      </ButtonGroup>
                  </div>
                  <div className="form-row">
                      <div className="form-group col-md-12">
                          <p className="menu-name" >Lesson Name:</p>
                          <p>{item && item.microLesson_Name}</p>
                      </div>
                  </div>

                  <div className="form-row">
                      <div className="form-group col-md-12">
                          <div dangerouslySetInnerHTML={{ __html: this.state.lessonContent }} />
                      </div>
                  </div>
              </Col>
          </div>
      )
  }


  runCodePrettify() {
      console.log({isCalled:'yes'})
      let script = document.createElement('script');
      script.type = 'text/javascript';
      script.async = true;
      script.src = 'https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
  }

  render() {
    const { editModelShow, courseDescription, lesson, key } = this.state
    return (
        <div>
            <div className="main_div">
                <Header/>
            </div>
            <div className="container">
                <div className="col-md-12 col-sm-12 col-xs-12 article_content">
                    <div>
                        <div className={'col-md-12 col-sm-12'}>
                            <div className="block_locker course-body">
                                <Row gutter={8}>
                                    <Col span={6} className="text-center">
                                        <Menu
                                            onClick={() =>this.handleClick}
                                            style={{ width: 256, textAlign: 'left'}}
                                            defaultSelectedKeys={['sub1']}
                                            defaultOpenKeys={['sub2']}
                                            mode="inline"
                                        >
                                            {/*<SubMenu key="sub1" title={<span><Icon type="mail" /><span>Introduction</span></span>} onClick={() => this.viewLessons(item, "Intro")}>
                                    </SubMenu>*/}
                                            <Menu.Item key="sub1">
                                                <p onClick={() => this.viewLessons(courseDescription && courseDescription.microCourse_Name, "Intro")}>Introduction</p>
                                            </Menu.Item>
                                            <SubMenu key="sub2" title={<span><Icon type="appstore" /><span>Lessons</span></span>}>
                                                {
                                                    courseDescription && courseDescription.microLessonsList.map((item,i) => (
                                                            <Menu.Item key={i} title={item.microLesson_Name}>
                                                                <div style={{ textAlign: 'left'}}
                                                                     onClick={() => this.viewLessons(item, "Lesson")}>{item.microLesson_Name}</div>
                                                            </Menu.Item>
                                                        )
                                                    )
                                                }
                                            </SubMenu>
                                        </Menu>
                                    </Col>

                                    {
                                        key === "Intro" ?
                                            <div>
                                                <Col span={18}>
                                                    <div className="form-row col-md-12">
                                                        <ButtonGroup>
                                                            <Button type="primary" onClick={() => this.handleBack()}>
                                                                <Icon type="left" />Back to Courses
                                                            </Button>
                                                        </ButtonGroup>
                                                    </div>
                                                    <div className="form-row">
                                                        <div className="form-group col-md-12">
                                                            <p className="menu-name">Course Name:</p>
                                                            <p>{courseDescription && courseDescription.microCourse_Name}</p>
                                                        </div>
                                                    </div>
                                                    <div className="form-row">
                                                        <div className="form-group col-md-12">
                                                            <p>Please Select a Lessons from the List</p>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </div> :
                                        this.showMenu(lesson, key)
                                    }
                                </Row>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
  }
}

export default courseHome;
